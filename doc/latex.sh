#!/bin/bash

latex -interaction=nonstopmode handbook.tex > .tmp
if [ $? -eq 0 ]
then
    dvipdfm handbook.dvi &> /dev/null
    # scp handbook.pdf host:~/Desktop/ > /dev/null
    # scp handbook.tex host:~/code/nixie_clock/doc > /dev/null
    rm .tmp
else
    cat .tmp
fi
    
