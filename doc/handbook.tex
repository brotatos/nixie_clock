\documentclass[titlepage]{article}
\usepackage{courier}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{fullpage}
\usepackage{hhline}
\usepackage{tabularx}
\usepackage{mdframed}
\usepackage{titlesec}
\newcommand{\sectionbreak}{\clearpage}
\newcommand{\infosign}{\fontencoding{U}\fontfamily{futs}\huge\selectfont\char 116\relax}
\newcommand{\warningsign}{\fontencoding{U}\fontfamily{futs}\Large\selectfont\char 66\relax}
\newcommand{\dangersign}{\fontencoding{U}\fontfamily{futs}\huge\selectfont\char 76\relax}

\newmdenv[linecolor=black,skipabove=\topsep,skipbelow=\topsep,
leftmargin=0pt,rightmargin=0pt,
innerleftmargin=5pt,innerrightmargin=5pt]{infobox}

\begin{document}
\title{\huge{Nixie Clock\\ Assembly Manual}}
\author{
  Jacob Hladky\\
  Computer Engineering Department\\
  California Polytechnic State University\\
  San Luis Obispo, CA\\
  \texttt{jhladky@calpoly.edu}
  \and
  Anlang Lu\\
  Electrical Engineering Department\\
  California Polytechnic State University\\
  San Luis Obispo, CA\\
  \texttt{alu06@calpoly.edu}
}
\vfill
\date{\today}
\maketitle

\pagenumbering{roman}

\tableofcontents
\clearpage

\pagenumbering{arabic}

\section{Introduction}
Hi! Welcome to the nixie clock assembly manual. The nixie clock project is a workshop offered by the Cal Poly IEEE Student Branch, intended as an introduction to soldering and microcontrollers concepts. This guide is written as a companion for that workshop, but it can be used on its own if you so desire.\\\\
This guide will show, step by step, how to solder and assemble the nixie clock, and then how to build its companion source code, and how to program that source code onto the board. It is indended for students who already have soldering experience, and who have a passing familiarity with some programming concepts.\\\\
Certain sections in this guide will refer to you external resources on the internet. If you feel you understand the concepts being discussed in the guide, feel free to ignore the links. However if you encounter a lot of terms you are unfamiliar with, take the time and explore the referenced material. Lastly, if you encounter any other words or pgrases in this guide you don't recognize, \underline{google is your friend}. Often a simple search of the term in question will provide sufficient information to clear up any misunderstanding. \\\\
Good luck and have fun!\\
\indent --- Jacob Hladky and Anlang Lu

\subsection{Formatting Used in this Guide}
\begin{itemize}
\item All code examples in this guide will be in the \texttt{courier} font.

\item Shell commands and urls will be in \emph{italics} if used inline. Shell commands only will be in the \texttt{courier} font and preceded by a \texttt{\#} if used as part of a larger example.

\item This is what a code example looks like:
\begin{verbatim}
ISR(TIMER0_COMPA_vect) {
   digit(i, timeToDigit(i, disp));
   i = i < NUM_DIGITS - 1 ? i + 1 : 0;
}
\end{verbatim}

\item This is what a shell example looks like: 
\begin{verbatim}
# make && make flash
\end{verbatim}
\end{itemize}

\begin{infobox}
  {\infosign} This is an info box. Info boxes contain helpful tips and links to other resources to help you if you get confused. If you know what you are doing, it is generally safe to ignore info boxes.
\end{infobox}

\begin{infobox}
  {\warningsign} This is a warning box. Warning boxes are places where should stop and double-check your work. Generally they serve to remind you to read an instruction carefully or of an action that may damage the project, or inconvenience yourself.
\end{infobox}

\begin{infobox}
  {\dangersign} This is a danger box. Danger boxes alert you to actions which may cause catastrophic damage to the project or real physical harm to your person. Ignore at your own risk.
\end{infobox}

% If you don't know what any of that means, don't worry! It will all be explained later.

\subsection{External Links}
\begin{itemize}
\item This project is licensed under the MIT license, and its source code -- including the \LaTeX source for this guide -- is available at\\
  \indent \emph{http://github.com/CalPolyIEEE/nixie\_clock}
\item A PDF of this guide is available at\\
  \indent \emph{http://static.calpolyieee.org/nixie\_clock\_guide.pdf}
\item A digital parts manifest, including links to where individual parts can be purchased, is availale at\\
  \indent \emph{http://static.calpolyieee.org/nixie\_clock\_parts.pdf}
\end{itemize}


\subsection{Errors}
No program is without bugs, and no manual is without typos. This project is no exception. If you encounter a typo in this guide or bug in the nixie clock software, please contact the authors as soon as possible so it can be fixed.

\section{Prerequsites}
\subsection{Tools}

You will need a soldering kit to build the nixie clock, which typically includes:
\begin{itemize}
\item 0.031'' 60/40 Rosin Core solder or similar
\item A soldering iron with adjustable temperature control e.g. Weller WES51
\item A damp sponge or gold foil
\item Fume extractor --- optional if you don't care about your lungs
\item Solder wick
\item Rubbing Alcohol
\end{itemize}
\hfill\\
The following are necessary to solder the surface mount chips on the board:
\begin{itemize}
\item Solder flux
\item Magnifying glass or equivalent
\item Tweezers
\item A good light source --- You will need it when inspecting small joints
\end{itemize}
\hfill\\
You will need the following to assemble the clock into its case:
\begin{itemize}
\item A file
\item A hot glue gun
\end{itemize}
\hfill\\
Lastly, there are also several miscellaneous tools you should also have on hand:
\begin{itemize}
\item A multimeter
\item Wire cutters
\item Third hands or other platform for holding components
\end{itemize}
\clearpage

\subsection{Parts Manifest}
The contents of the nixie clock kit are listed here. The ``item'' column contains an description of the item. The ``Distributor Code and Part No.'' column contains information for obtaining individual parts online. The ``code'' maps to an online parts distributor and the ``Part No.'' is the \emph{distributor}'s own part number for the item. This information should be sufficient to uniquely identify and purchase any of these parts online.\\\\
Make sure to double check with this manifest \emph{before} you start building.

\subsubsection{Distributor Codes}
\begin{tabular}{|l|l|l|}
\hline
\textbf{Code} & \textbf{Distributor} & \textbf{URL} \\ \hhline{|=|=|=|}
DK & Digikey  & \emph{http://www.digikey.com}  \\ \hline
AF & Adafruit & \emph{http://www.adafruit.com} \\ \hline
AZ & Amazon   & \emph{http://www.amazon.com}   \\ \hline
OP & OSH Park & \emph{http://www.oshpark.com}  \\ \hline
\end{tabular}

\subsubsection{Parts}
\begin{tabularx}{\textwidth}{|r|X|X|}
\hline
\textbf{Quantity} & \textbf{Item} & \textbf{Distributor Code and Part No.} \\ \hhline{|=|=|=|}
1 & Printed circuit board          & OP XXXXXX           \\ \hline
1 & IV-18 Nixie tube               & AF 242              \\ \hline
1 & ``Ice-Tube'' side PCB          & AF 343              \\ \hline
1 & 3D-printed Case Base Component & \emph{n/a}          \\ \hline
1 & 3D-printed Case Top Component  & \emph{n/a}          \\ \hline
1 & ATMega328P Microcontroller     & DK ATMEGA328P-AU-ND \\ \hline
2 & Ribbon Cable Socket Connector  & DK HKC20H-ND        \\ \hline
1 & 3''-6'' Ribbon Cable           & DK AE20G-300-ND     \\ \hline
1 & On-Off-On Slide Switch         & DK 679-1867-ND      \\ \hline
1 & Piezoelectric Buzzer           & DK 445-2525-1-ND    \\ \hline
2 & 22pF Ceramic Capacitor         & DK 712-1275-1-ND    \\ \hline
5 & 10k$\Omega$ Resistor           & DK RHM10.0KCDCT-ND  \\ \hline
1 & 240$\Omega$ Resistor           & DK P240JCT-ND       \\ \hline
1 & VFD Controller                 & DK MAX6921AUI+-ND   \\ \hline
1 & 40V NPN BJT Transistor         & DK 2N3053-ND        \\ \hline
1 & 16Mhz Crystal Oscillator       & DK X1103-ND         \\ \hline
1 & Micro USB Type-B Connector     & DK H11890-ND        \\ \hline
1 & Diode                          & DK 641-1310-1-ND    \\ \hline
3 & Extra-Long Dual-Pole button    & AF 1490             \\ \hline
1 & 1uF electrolytic capacitor     & DK XXXXX            \\ \hline
1 & Tri-color LED                  & DK XXXXX            \\ \hline
1 & 680uH Inductor                 & DK XXXXX            \\ \hline
1 & Extra-Large red LED / button   & AF 1439             \\ \hline
2 & 2x3 male header                & DK S2011EC-03-ND    \\ \hline
2 & 2x10 male header               & DK S2011EC-10-ND    \\ \hline
1 & Optional -- Cylindrical Weights & AZ XXXXX            \\ \hline
\end{tabularx}
\clearpage

\section{Software Build Environment}
\begin{infobox}
  {\infosign} This guide assumes that you are using a computer running GNU/Linux or some POSIX compliant variant. If you're taking the IEEE nixie clock workshop, your instructor should help you at this point. On campus resources for linux information are the Cal Poly Linux Users Group (CPLUG) [\emph{http://cplug.org}] and The WhiteHat, a cybersecurity club [\emph{http://thewhitehat.club}].\\\\
  For more immediate information about linux, try an online introduction such as this:\\
  \indent \emph{http://www.linux.com/learn/tutorials/784060-the-complete-beginners-guide-to-linux}\\
  or this online course on linux:\\
  \indent \emph{https://www.udemy.com/introductiontolinux/}
\end{infobox}
The nixie clock software was written in C and compompiled with gcc-avr. It it flashed to the board with avrdude.


\section{Construction and Assembly}
asasa

\end{document}
