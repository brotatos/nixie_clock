#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>

#define STATUS_LED (1 << PC1)

int main() {

   DDRC = 0 | STATUS_LED;

   while (true) {
      PORTC |= STATUS_LED;
      _delay_ms(1000);
      PORTC &= ~STATUS_LED;
      _delay_ms(1000);
   }

   return 0;
}
