#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include <stdint.h>

static inline void spi_init();
static void spi_transmit(uint8_t data);

#define LATCH (1 << PB2)
#define STATUS_LED (1 << PC1)
#define BLANK (1 << PD7)

int main() {

   DDRB = 0 | LATCH;
   DDRC = 0 | STATUS_LED;
   DDRD = 0 | BLANK;

   PORTD &= ~BLANK;
   
   PRR &= ~(1 << PRTIM1);
   TCCR1A |= (1 << COM1A0); // (1)
   TCCR1B |= (1 << WGM12) | (1 << CS10); // (2)
   OCR1A = 499;
   
   spi_init();
   
   //how to verify the tube is working here... ???

   PORTB &= ~LATCH; //hold latch low while we're transmitting
   spi_transmit(0xFF);
   spi_transmit(0xFF);
   spi_transmit(0xFF);

   //latch the LATCH pin
   PORTB |= LATCH;
   PORTB &= ~LATCH;
   
   while (true) {
      PORTC |= STATUS_LED;
      _delay_ms(1000);
      PORTC &= ~STATUS_LED;
      _delay_ms(1000);
   }

   return 0;
}

static inline void spi_init(void) {
   //Set MOSI and SCLK outputs
   DDRB |= (1 << DDB3) | (1 << DDB5); //MOSI is PB3, SCK is PB5

   //Enable SPI, Master, set clock rate
   SPCR |= (1 << SPE) | (1 << MSTR); //enable SPI master
   SPSR |= (1 << SPI2X); //double transmissing rate
}

static void spi_transmit(uint8_t data) {
   //start transmission
   SPDR = data;

   while(!(SPSR & (1 << SPIF)))
      ;
}
