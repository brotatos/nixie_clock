/* Nixie Clock Project
 * IEEE Student Branch -- Cal Poly
 * Fall Quarter 2014
 * JACOB HLADKY / ANLANG LU
 *
 * The tube code.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <stdint.h>
#include "tube.h"

/*
 * Globals.
 */
static volatile struct hms time = {0, 0, 0}, alarm = {0, 0, 0};

// points to the hms struct that should be shown on the clock
static volatile struct hms* disp = &time;
static volatile bool alarmOn = false; // indicates whether the alarm is enabled

int main() {
   bool timeCfgFirstRun = true, alrmCfgFirstRun = true;
   uint16_t alrmTmpCnt = 0;

   /*
    * (1) Set up all other pins and registers
    * (2) Set up the SPI module
    * (3) Enable global interrupts i.e. start operation
    */
   tube_init(); // (1)
   spi_init(); // (2)
   sei(); // (3)

   //hack
   DDRD |= (1 << PD5);
   PORTD |= (1 << PD5);
   //end hack

   //PORTC |= ALRM;

   /*
    * The control loop.
    */
   while (true) {
      while (PIND & TIME_SET) {
         if (timeCfgFirstRun) {
            PRR |= (1 << PRTIM2); // disable time
            timeCfgFirstRun = false;
         }
         hms_set(&time);
      }
      if (timeCfgFirstRun == false) {
         timeCfgFirstRun = true;
         PRR &= ~(1 << PRTIM2); // reenable time
      }
      while (PIND & ALRM_SET) { //code to handle alarm setting here
         if (alrmCfgFirstRun) {
            alrmCfgFirstRun = false;
            disp = &alarm;
         }
         hms_set(&alarm);
      }
      if (alrmCfgFirstRun == false) {
         alrmCfgFirstRun = true;
         disp = &time;
      }
      if (PINC & ALRM_BTN) {
         alrmTmpCnt++;
      }
      if (alrmTmpCnt == DEBOUNCE_MAX) {
         alrmTmpCnt = 0;
         alarmOn = !alarmOn;
      }
   }
   return 0;
}

ISR(TIMER2_COMPA_vect) {
   static uint16_t ledCnt = 0;

   if (ledCnt == LED_MAX) { //exactly equal to once a second
      ledCnt = 0;
      time.sec++;
      PORTC ^= STATUS_LED;
      if (time.sec == SEC_MAX) {
         time.min++;
         time.sec = 0;
      }
      if (time.min == MIN_MAX) {
         time.hr++;
         time.min = 0;
      }
      if (time.hr == HR_MAX) {
         time.hr = 0;
      }
   }
   ledCnt++;

   /*
    * This is a hack that sets the ALRM_LED pin depending on alarmOn.
    * See: http://graphics.stanford.edu/~seander/bithacks.html#\
    *      ConditionalSetOrClearBitsWithoutBranching
    */
   PORTC ^= (-alarmOn ^ PORTC) & ALRM_LED;
}

ISR(TIMER0_COMPA_vect) {
   static uint8_t i = 0;

   digit(i, time_to_digit(i, disp));
   i = i < NUM_DIGITS - 1 ? i + 1 : 0;
}

//indicates a bad (unhandled) interrupt was caught
ISR(BADISR_vect) {
   PORTC |= EMRG_LED;
}

static inline void tube_init() {
   /*
    * Configure GPIO registers.
    * DDRA: Not used.
    * DDRB: Set LATCH and PWM_OUT to output, and all other pins to input.
    * DDRC: Set ALRM, ALRM_LED and STATUS_LED to output, and set ALRM_BTN,
    *       along with all other pins, to input.
    * DDRD: Set BLANK to output, and set TIME_SET, ALRM_SET, HR_IN, MIN_IN,
            SEC_IN, along with all other pins, to input.
    */
   DDRB = 0 | LATCH | PWM_OUT;
   DDRC = 0 | ALRM | ALRM_LED | STATUS_LED;
   DDRD = 0 | BLANK;

   /*
    * Configure Timer0.
    * (1) Set the timer to clear when there is a compare match.
    * (2) Prescale Timer0 by 1024.
    * (3) Generate an interrupt when there is a match with the
    *     value in OCR0A.
    */
   PRR &= ~(1 << PRTIM0);
   TCCR0A |= (1 << WGM01); // (1)
   TCCR0B |= (1 << CS02) | (1 << CS00);  // (2)
   TIMSK0 |= (1 << OCIE0A); // (3)
   OCR0A = 0;

   /*
    * Configure Timer1.
    * (1) Toggle OC1A (a GPIO pin) when the timer value equals
    *     the value in OCR1A.
    * (2) Clear the timer value when there is a compare match.
    *     Do not prescale Timer1.
    */
   PRR &= ~(1 << PRTIM1);
   TCCR1A |= (1 << COM1A0); // (1)
   TCCR1B |= (1 << WGM12) | (1 << CS10); // (2)
   OCR1A = 499;

   /*
    * Configure Timer2, which has very similar settings as Timer0.
    */
   PRR &= ~(1 << PRTIM2);
   TCCR2A |= (1 << WGM21);
   TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20);
   TIMSK2 |= (1 << OCIE2A);
   OCR2A = 0;

   /*
    * (1) Turn STATUS_LED on to indicate normal operation.
    * (2) Turn blank off i.e. enable display output on the tube.
    */
   PORTC |= STATUS_LED; // (1)
   PORTD &= ~BLANK; // (2)
};

static void hms_set(volatile struct hms* t) {
   static uint16_t hrTmpCnt = 0, minTmpCnt = 0, secTmpCnt = 0;

   if (PIND & HR_IN) {
      hrTmpCnt++;
   }
   if (PIND & MIN_IN) {
      minTmpCnt++;
   }
   if (PIND & SEC_IN) {
      secTmpCnt++;
   }
   if (hrTmpCnt > DEBOUNCE_MAX) {
      t->hr++;
      if (t->hr > HR_MAX - 1) {
         t->hr = 0;
      }
      hrTmpCnt = 0;
   }
   if (minTmpCnt > DEBOUNCE_MAX) {
      t->min++;
      if (t->min > MIN_MAX - 1) {
         t->min = 0;
      }
      minTmpCnt = 0;
   }
   if (secTmpCnt > DEBOUNCE_MAX) {
      t->sec++;
      if (t->sec > SEC_MAX) {
         t->sec = 0;
      }
      secTmpCnt = 0;
   }
}

static inline char time_to_digit(uint8_t pos, volatile struct hms* t) {
   switch(pos) {
   case 0: return '0' + t->sec % 10;
   case 1: return '0' + t->sec / 10;
   case 3: return '0' + t->min % 10;
   case 4: return '0' + t->min / 10;
   case 6: return '0' + t->hr % 10;
   case 7: return '0' + t->hr / 10;
   default:
   case 2:
   case 5:
   case 8: return '~';
   }
}

/*
 * (1) Turn on the big dot in position 8
 * (2) ~ is off because it's clearly impossible to represent
 */
static void digit(uint8_t pos, char digit) {
   uint8_t digMap, tmp;

   switch(digit) { //supports all hex values + '.'
   case '0': digMap = 0x3Fu; break;
   case '1': digMap = 0x06u; break;
   case '2': digMap = 0x5bu; break;
   case '3': digMap = 0x4fu; break;
   case '4': digMap = 0x66u; break;
   case '5': digMap = 0x6du; break;
   case '6': digMap = 0x7du; break;
   case '7': digMap = 0x07u; break;
   case '8': digMap = 0x7fu; break;
   case '9': digMap = 0x67u; break;
   case 'a':
   case 'A': digMap = 0x77u; break;
   case 'b':
   case 'B': digMap = 0x7cu; break;
   case 'c':
   case 'C': digMap = 0x58u; break;
   case 'd':
   case 'D': digMap = 0x5eu; break;
   case 'e':
   case 'E': digMap = 0x79u; break;
   case 'f':
   case 'F': digMap = 0x71u; break;
   case 'g':
   case 'G': digMap = 0x3du; break;
   case 'h':
   case 'H': digMap = 0x74u; break;
   case 'i':
   case 'I': digMap = 0x30u; break;
   case 'j':
   case 'J': digMap = 0x1eu; break;
   case 'l':
   case 'L': digMap = 0x38u; break;
   case 'n':
   case 'N': digMap = 0x54u; break;
   case 'o':
   case 'O': digMap = 0x5cu; break;
   case 'p':
   case 'P': digMap = 0x73u; break;
   case 'r':
   case 'R': digMap = 0x50u; break;
   case 's':
   case 'S': digMap = 0x6du; break;
   case 't':
   case 'T': digMap = 0x78u; break;
   case 'u':
   case 'U': digMap = 0x1cu; break;
   case 'y':
   case 'Y': digMap = 0x6eu; break;
   case '.': digMap = 0x80u; break; // (1)
   default:
   case '~': digMap = 0x00u; break; // (2)
   }

   tmp = (digMap & 0x0F) << 4;
   PORTB &= ~LATCH; //hold latch low while we're transmitting
   spi_transmit((digMap & 0xF0) >> 4);
   spi_transmit(pos == NUM_DIGITS ? tmp | 0x01 : tmp);
   spi_transmit(pos < NUM_DIGITS ? 1 << pos : 0);

   //latch the LATCH pin
   PORTB |= LATCH;
   PORTB &= ~LATCH;
}

//Check for hms struct equality
static inline bool hms_eq(volatile struct hms* a, volatile struct hms* b) {
   return a->hr == b->hr && a->min == b->min && a->sec == b->sec;
}

static inline void spi_init(void) {
   //Set MOSI and SCLK outputs
   DDRB |= (1 << DDB3) | (1 << DDB5); //MOSI is PB3, SCK is PB5

   //Enable SPI, Master, set clock rate
   SPCR |= (1 << SPE) | (1 << MSTR); //enable SPI master
   SPSR |= (1 << SPI2X); //double transmissing rate
}

static void spi_transmit(uint8_t data) {
   //start transmission
   SPDR = data;

   while(!(SPSR & (1 << SPIF)))
      ;
}
