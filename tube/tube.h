#ifndef TUBE_H
#define TUBE_H

/*
 * Pin defines.
 */
#define PWM_OUT (1 << PB1)         /* signal for boost converter */
#define LATCH (1 << PB2)           /* tube controller latch pin */
#define EMRG_LED (1 << PC0)        /* debugging LED; indicates error condition */
#define STATUS_LED (1 << PC1)      /* status LED */
#define EXTRA_LED (1 << PC2)       /* no purpose as of this time */
#define ALRM (1 << PC3)            /* the alarm buzzer */
#define ALRM_BTN (1 << PC4)        /* the big red alarm button */
#define ALRM_LED (1 << PC5)        /* the big red alarm LED */
#define TIME_SET (1 << PD0)        /* time set switch */
#define ALRM_SET (1 << PD1)        /* alarm set switch */
#define HR_IN (1 << PD2)           /* hrs button */
#define MIN_IN (1 << PD3)          /* minutes button */
#define SEC_IN (1 << PD4)          /* seconds button */
#define BLANK (1 << PD7)           /* tube controller blank */

/*
 * Other defines.
 */
#define LED_MAX 15625U             /* the value of ledCnt equal to 1 second */
#define DEBOUNCE_MAX 50000U        /* clock cycles to wait until registering input */
#define NUM_DIGITS 9               /* number of 'digits' in the clock */
#define SIZE_BYTE 8                /* bits in a byte */
#define HR_MAX 24                  /* hours in a day */
#define MIN_MAX 60                 /* minutes in an hour */
#define SEC_MAX 60                 /* seconds in a minute */
#define BTM_MASK 0x0F              /* mask out the top half of a byte */
#define TOP_MASK 0xF0              /* mask out the bottom half of a byte */

/*
 * Typedefs, enums, structs, and unions.
 */
struct hms {
   uint8_t hr;  /* hrs since midnight */
   uint8_t min; /* mins since midnight */
   uint8_t sec; /* secs since midnight */
} hms;

static inline void tube_init();
static inline void spi_init();
static void spi_transmit(uint8_t data);
static void digit(uint8_t pos, char digit);
static inline char time_to_digit(uint8_t pos, volatile struct hms* t);
static void hms_set(volatile struct hms* t);
static inline bool hms_eq(volatile struct hms* a, volatile struct hms* b);

#endif
