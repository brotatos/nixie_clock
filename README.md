The Nixie Clock
===============

* doc/: Contains documentation for this project, a latex handbook for assembling and programming the nixie clock. To build the handbook run the `latex.sh` script. A PDF copy of the guidebook is also available at http://static.calpolyieee.org/nixie_clock_guide.pdf
* tube/ -- Contains the main program for the tube.
* test/ -- Contains various test programs for the clock to verify functionality.

For more information please see the handbook.
